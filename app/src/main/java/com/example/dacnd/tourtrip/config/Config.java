package com.example.dacnd.tourtrip.config;

/**
 * Created by Dac ND on 13-Dec-17.
 */

public class Config{
        public static final String END_POINT = "http://192.168.1.111";//MYANMAR-
        public static final String END_POINT_RESETPW = "http://203.81.70.141:8123";
        public static final String PREFIX = "95";
        public static final String APPCODE = "mbccs";
        public static final String hasNIMS = "0";
        public static final int TUOI = 15;
        public static final String DATE_FORMAT = "dd/MM/yyyy";
        public static final String TIMEZONE_FORMAT_SERVER = "yyyy-MM-dd'T'HH:mm:ssZ";
        public static final int MAX_IMAGE_WITH_COMPRESS = 640;
        public static final int MAX_IMAGE_HEIGHT_COMPRESS = 480;
        public static final int IMAGE_COMPRESS_QUANTITY = 75;
        public static final long STAFF_EXPORT_SHOP_REASON_ID = 200383L;
        public static final int NUMBER_SELECT_IMAGE = 3;
        public static final String COUNTRY_NAME = "Myanmar";
        public static final String DB_AREAS_FILE_NAME = "db_area_bur.json";
        public static final String DEFAULT_LANGUAGE = "eng";
        public static final String DEFAULT_CURRENCY = "VND";
        public static final String[] AVAILABLE_LANGUAGE_CODES = {"vie", "eng", "mya"};
        public static final int AUTO_COMPLETE_THRESHOLD = 1;
}
