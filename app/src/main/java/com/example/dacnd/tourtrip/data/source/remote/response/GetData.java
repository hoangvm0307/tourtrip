package com.example.dacnd.tourtrip.data.source.remote.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.dacnd.tourtrip.data.model.database.Diadiem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Dac ND on 22-Dec-17.
 */

public class GetData implements Parcelable {
    @SerializedName("điaiems")
    @Expose
    private List<Diadiem> diadiems;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.diadiems);
    }

    public GetData() {
    }

    protected GetData(Parcel in) {
        this.diadiems = in.createTypedArrayList(Diadiem.CREATOR);
    }

    public static final Parcelable.Creator<GetData> CREATOR = new Parcelable.Creator<GetData>() {
        @Override
        public GetData createFromParcel(Parcel source) {
            return new GetData(source);
        }

        @Override
        public GetData[] newArray(int size) {
            return new GetData[size];
        }
    };

    public List<Diadiem> getDiadiems() {
        return diadiems;
    }

    public void setDiadiems(List<Diadiem> diadiems) {
        this.diadiems = diadiems;
    }

    public static Creator<GetData> getCREATOR() {
        return CREATOR;
    }
}
