package com.example.dacnd.tourtrip.screen.splash.setting;

import android.content.Context;

import com.example.dacnd.tourtrip.data.model.database.Diadiem;
import com.example.dacnd.tourtrip.data.source.remote.service.Api_CLient;
import com.example.dacnd.tourtrip.data.source.remote.service.TourTripAPI;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Dac ND on 16-Dec-17.
 */

public class SettingPresenter implements SettingContact.Prsenter {
    private SettingContact.ViewModel mViewmodel;
    private Context mContext;
    private DiadiemAdapter mAdapter;
    private List<Diadiem> mDiadiems;

    public SettingPresenter(final SettingContact.ViewModel mViewmodel, final Context mContext) {
        this.mViewmodel = mViewmodel;
        this.mContext = mContext;
        getDulieuSanphammoinhat();
        mDiadiems = new ArrayList<>();
        mAdapter = new DiadiemAdapter(mContext,mDiadiems);
        mAdapter.setItemListemer(new DiadiemAdapter.ItemListener() {
            @Override
            public void onClick(int position, Diadiem item) {
                mViewmodel.goTomap(mDiadiems.get(position));
            }
        });
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {

    }
    public DiadiemAdapter getAdapter() {
        return mAdapter;
    }
    private void getDulieuSanphammoinhat() {
        TourTripAPI api = Api_CLient.getRetrofit().create(TourTripAPI.class);
        Call<ArrayList<Diadiem>> call = api.getLoaisanpham();
        call.enqueue(new Callback<ArrayList<Diadiem>>() {
            @Override
            public void onResponse(Call<ArrayList<Diadiem>> call, Response<ArrayList<Diadiem>> response) {
                for(Diadiem  a : response.body()){
                    mDiadiems.add(a);
                }
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<ArrayList<Diadiem>> call, Throwable t) {

            }
        });
    }
}
