package com.example.dacnd.tourtrip.data.model.database;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.DrawableRes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dac ND on 15-Dec-17.
 */

public class Function implements Parcelable {
    @DrawableRes
    private int mIcon;
    private int orderIndex;
    @SerializedName("functionCode")
    @Expose
    private String functionCode;
    @SerializedName("functionName")
    @Expose
    private String functionName;

    public int getmIcon() {
        return mIcon;
    }

    public void setmIcon(int mIcon) {
        this.mIcon = mIcon;
    }

    public int getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(int orderIndex) {
        this.orderIndex = orderIndex;
    }

    public String getFunctionCode() {
        return functionCode;
    }

    public void setFunctionCode(String functionCode) {
        this.functionCode = functionCode;
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mIcon);
        dest.writeInt(this.orderIndex);
        dest.writeString(this.functionCode);
        dest.writeString(this.functionName);
    }

    public Function() {
    }

    public Function(int mIcon, int orderIndex, String functionCode, String functionName) {
        this.mIcon = mIcon;
        this.orderIndex = orderIndex;
        this.functionCode = functionCode;
        this.functionName = functionName;
    }

    protected Function(Parcel in) {
        this.mIcon = in.readInt();
        this.orderIndex = in.readInt();
        this.functionCode = in.readString();
        this.functionName = in.readString();
    }

    public static final Parcelable.Creator<Function> CREATOR = new Parcelable.Creator<Function>() {
        @Override
        public Function createFromParcel(Parcel source) {
            return new Function(source);
        }

        @Override
        public Function[] newArray(int size) {
            return new Function[size];
        }
    };

}
