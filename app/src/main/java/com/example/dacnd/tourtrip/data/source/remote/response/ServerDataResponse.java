package com.example.dacnd.tourtrip.data.source.remote.response;

/**
 * Created by Dac ND on 22-Dec-17.
 */

public class ServerDataResponse<T> extends DataResponse {

    private T result;

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }
}
