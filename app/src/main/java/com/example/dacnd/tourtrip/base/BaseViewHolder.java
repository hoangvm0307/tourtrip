package com.example.dacnd.tourtrip.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

/**
 * Created by Dac ND on 14-Dec-17.
 */

public class BaseViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            public BaseViewHolder(View itemView) {
                super(itemView);
                itemView.setOnClickListener(this);
                ButterKnife.bind(this, itemView);
            }

            @Override
            public void onClick(View v) {

            }
        }

