package com.example.dacnd.tourtrip.base;

/**
 * Created by Dac ND on 13-Dec-17.
 */

public interface BasePresenter {
    //subscribe event
    void subscribe();

    //unsubscribe event
    void unSubscribe();
}
