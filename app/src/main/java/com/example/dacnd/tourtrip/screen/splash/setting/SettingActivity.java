package com.example.dacnd.tourtrip.screen.splash.setting;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.dacnd.tourtrip.R;
import com.example.dacnd.tourtrip.base.BaseDataBindActivity;
import com.example.dacnd.tourtrip.data.model.database.Diadiem;
import com.example.dacnd.tourtrip.databinding.FragmentSettingBinding;
import com.example.dacnd.tourtrip.screen.splash.map.Map;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dac ND on 16-Dec-17.
 */

public class SettingActivity extends BaseDataBindActivity<FragmentSettingBinding,SettingPresenter>
    implements SettingContact.ViewModel{
    List<Diadiem> mangsanPham= new ArrayList<>();;
    RecyclerView recyclerview;
    SanPhamAdapter sanphamadapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    protected int getIdLayout() {
        return R.layout.fragment_setting;
    }

    @Override
    protected void initData() {
        recyclerview = mBinding.recyclerview;
        mPresenter = new SettingPresenter(this,this);
        recyclerview.setHasFixedSize(true);
        recyclerview.setLayoutManager(new GridLayoutManager(getApplicationContext(),1));
        mBinding.setPresenter(mPresenter);

    }

    @Override
    public void goTomap(Diadiem diadiem) {
        Intent intent = new Intent(this, Map.class);
        intent.putExtra("lat",Double.parseDouble(diadiem.getLat()));
        intent.putExtra("lon",Double.parseDouble(diadiem.getLon()));
        intent.putExtra("name",diadiem.getTen_dd());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
