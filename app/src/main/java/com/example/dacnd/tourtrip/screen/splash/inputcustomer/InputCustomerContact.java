package com.example.dacnd.tourtrip.screen.splash.inputcustomer;

import com.example.dacnd.tourtrip.base.BasePresenter;
import com.example.dacnd.tourtrip.base.BaseView;

/**
 * Created by Dac ND on 14-Dec-17.
 */

public class InputCustomerContact {
    interface Presenter extends BasePresenter{

    }
    interface ViewModel extends BaseView<Presenter>{
        void ClickBack();
        void doProcess();
    }
}
