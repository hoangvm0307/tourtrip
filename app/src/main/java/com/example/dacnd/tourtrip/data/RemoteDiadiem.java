package com.example.dacnd.tourtrip.data;

import com.example.dacnd.tourtrip.data.model.database.Diadiem;
import com.example.dacnd.tourtrip.data.source.remote.IRenoteDiadiem;
import com.example.dacnd.tourtrip.data.source.remote.remote.GetDataRenoteDiadiem;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by Dac ND on 22-Dec-17.
 */

public class RemoteDiadiem  implements IRenoteDiadiem{
    private static  RemoteDiadiem instance;
    private GetDataRenoteDiadiem getDataRenoteDiadiem;

    public RemoteDiadiem(GetDataRenoteDiadiem getDataRenoteDiadiem) {
        this.getDataRenoteDiadiem = getDataRenoteDiadiem;
    }

    @Override
    public Observable<ArrayList<Diadiem>> getData() {
        return getDataRenoteDiadiem.getData();
    }
    public static  RemoteDiadiem getInstance(){
        if(instance == null){
            instance = new RemoteDiadiem(GetDataRenoteDiadiem.getIntances());
        }
        return  instance;
    }


}
