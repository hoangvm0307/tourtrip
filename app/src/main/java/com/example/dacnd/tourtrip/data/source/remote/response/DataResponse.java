package com.example.dacnd.tourtrip.data.source.remote.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Dac ND on 22-Dec-17.
 */

public class DataResponse {
    @SerializedName("errorCode")
    private String errorCode;

    @SerializedName("errorMessage")
    private String errorMessage;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
