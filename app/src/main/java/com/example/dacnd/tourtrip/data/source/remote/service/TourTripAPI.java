package com.example.dacnd.tourtrip.data.source.remote.service;

import com.example.dacnd.tourtrip.data.model.database.Diadiem;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by Dac ND on 14-Dec-17.
 */

public interface TourTripAPI {
    @GET("/klserver/getdd.php")
    Call<ArrayList<Diadiem>> getLoaisanpham();

        @GET("/klserver/getdd.php")
        Observable<List<Diadiem>> getData();
}
