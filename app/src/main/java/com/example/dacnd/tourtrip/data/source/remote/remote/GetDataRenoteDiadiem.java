package com.example.dacnd.tourtrip.data.source.remote.remote;

import com.example.dacnd.tourtrip.data.model.database.Diadiem;
import com.example.dacnd.tourtrip.data.source.remote.IRenoteDiadiem;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by Dac ND on 22-Dec-17.
 */

public class GetDataRenoteDiadiem implements IRenoteDiadiem {
    private static GetDataRenoteDiadiem intances;
    @Override
    public Observable<ArrayList<Diadiem>> getData() {
        return null;

    }
    public static GetDataRenoteDiadiem getIntances(){
        if(intances == null){
            intances = new GetDataRenoteDiadiem();
        }
        return intances;
    }
}
