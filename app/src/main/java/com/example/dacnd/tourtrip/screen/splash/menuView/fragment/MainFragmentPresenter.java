package com.example.dacnd.tourtrip.screen.splash.menuView.fragment;

import android.content.Context;

/**
 * Created by Dac ND on 15-Dec-17.
 */

public class MainFragmentPresenter implements MainFragmentContact.Presenter {
    private MainFragmentContact.ViewModel mViewModel;
    private Context mContext;
    public MainFragmentPresenter(MainFragmentContact.ViewModel mViewModel, Context mContext) {
        this.mViewModel = mViewModel;
        this.mContext = mContext;
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {

    }
}
