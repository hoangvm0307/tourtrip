package com.example.dacnd.tourtrip.screen.splash;

import com.example.dacnd.tourtrip.base.BasePresenter;
import com.example.dacnd.tourtrip.base.BaseView;

/**
 * Created by Dac ND on 13-Dec-17.
 */

public class SplashModel {
    interface Presenter extends BasePresenter{

    }
    interface ViewModel extends BaseView<Presenter>{

    }
}
