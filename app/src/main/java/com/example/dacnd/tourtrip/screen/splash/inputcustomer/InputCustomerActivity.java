package com.example.dacnd.tourtrip.screen.splash.inputcustomer;

import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.example.dacnd.tourtrip.R;
import com.example.dacnd.tourtrip.base.BaseDataBindActivity;
import com.example.dacnd.tourtrip.databinding.ActivityInputCustomerBinding;
import com.example.dacnd.tourtrip.java.ListTrips;
import com.example.dacnd.tourtrip.screen.splash.menuView.MenuViewActivity;

/**
 * Created by Dac ND on 14-Dec-17.
 */

public class InputCustomerActivity extends BaseDataBindActivity<ActivityInputCustomerBinding,InputCustomerPresenter>
    implements InputCustomerContact.ViewModel{
    @Override
    public void showLoading() {
        showLoadingDialog();
    }

    @Override
    public void hideLoading() {
        hideLoadingDialog();
    }

    @Override
    protected int getIdLayout() {
        return R.layout.activity_input_customer;
    }

    @Override
    protected void initData() {
        mPresenter = new InputCustomerPresenter(this,this);
        mBinding.setPresenter(mPresenter);
    }

    @Override
    public void ClickBack() {
        finish();
    }

    @Override
    public void doProcess() {
//        startActivity(new Intent(this, MenuViewActivity.class));
        new GetListTrip().execute();

    }

    private class GetListTrip extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            /* TODO: 08/01/2018
            parse object
            */
            Log.e("listTrips",
                    ListTrips.getInstance().getData("https://www.booking.com/searchresults.html?label=gen173nr-1FCAEoggJCAlhYSDNYBGj0AYgBAZgBMcIBCndpbmRvd3MgMTDIAQzYAQHoAQH4AQKSAgF5qAID;sid=85fc651cb1fd9d509ad4ae64c4d5c366;age=12&age=12&checkin_month=1&checkin_monthday=4&checkin_year=2018&checkout_month=1&checkout_monthday=5&checkout_year=2018&class_interval=1&dest_id=-3714993&dest_type=city&dtdisc=0&from_sf=1&group_adults=1&group_children=2&inac=0&index_postcard=0&label_click=undef&no_rooms=1&offset=0&oos_flag=0&postcard=0&raw_dest_type=city&room1=A%2C12%2C12&sb_price_type=total&sb_travel_purpose=business&search_selected=1&show_non_age_message=1&src=index&src_elem=sb&ss=Hanoi%2C%20Ha%20Noi%20Municipality%2C%20Vietnam&ss_all=0&ss_raw=Hanoi&ssb=empty&sshis=0&").toString());
            return null;
        }
    }
}
