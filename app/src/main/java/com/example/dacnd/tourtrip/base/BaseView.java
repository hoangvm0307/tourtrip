package com.example.dacnd.tourtrip.base;

/**
 * Created by Dac ND on 13-Dec-17.
 */

public interface BaseView<T> {
    void showLoading();

    void hideLoading();
}
