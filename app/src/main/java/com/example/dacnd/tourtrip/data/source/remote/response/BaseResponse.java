package com.example.dacnd.tourtrip.data.source.remote.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dac ND on 22-Dec-17.
 */

public class BaseResponse<T> extends DataResponse {

    @Expose
    @SerializedName("object")
    private T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
