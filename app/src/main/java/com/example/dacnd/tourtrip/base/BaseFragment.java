package com.example.dacnd.tourtrip.base;

import android.support.v4.app.Fragment;

/**
 * Created by Dac ND on 14-Dec-17.
 */

public abstract class BaseFragment extends Fragment {

    public void showLoadingDialog() {
        showLoadingDialog(true);
    }

    public void hideLoadingDialog() {
        if (getActivity() != null && getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).hideLoadingDialog();
        }
    }

    public void showLoadingDialog(final boolean cancelable) {
        if (getActivity() != null && getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).showLoadingDialog(cancelable);
        }
    }
}

