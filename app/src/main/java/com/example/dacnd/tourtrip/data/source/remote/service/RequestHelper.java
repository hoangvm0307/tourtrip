package com.example.dacnd.tourtrip.data.source.remote.service;

import com.example.dacnd.tourtrip.BuildConfig;
import com.example.dacnd.tourtrip.config.Config;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Dac ND on 14-Dec-17.
 */

public class RequestHelper {

    public static TourTripAPI instance1;
    public static TourTripAPI instance2;
    public static TourTripAPI instance3;
    public static TourTripAPI instance4;

    private static final int CONNECTION_TIMEOUT = 30;

    public static TourTripAPI getRequest() {
        if (instance1 == null) {
            instance1 = getRequest(false, false);
        }
        return instance1;
    }

    // TODO: 12/06/2017 Fake request
    public static TourTripAPI getFakeRequest() {
        if (instance3 == null) {
            instance3 = getFakeRequest(false, false);
        }
        return instance3;
    }

    public static TourTripAPI getPWRequest() {
        if (instance4 == null) {
            instance4 = getPWRequestConnect();
        }
        return instance4;
    }

    public static TourTripAPI getRequestHeader() {
        return getRequest(true, false);
    }

    // TODO: 12/06/2017 Fake request
    public static TourTripAPI getFakeRequestHeader() {
        return getFakeRequest();
    }

    public static TourTripAPI getRequestMultipart() {
        if (instance2 == null) {
            instance2 = getRequest(false, true);
        }
        return instance2;
    }

    public static TourTripAPI getRequest(boolean isHeader, boolean isMultiPart) {
        HttpLoggingInterceptor logging = getHttpLoggingInterceptor();
        OkHttpClient client;
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        httpClient.readTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        httpClient.addInterceptor(logging);
        if (isHeader) {
            httpClient.addInterceptor(requestWithHeader());
        }
        if (isMultiPart) {
            httpClient.addInterceptor(requestMultipart());
        }

        Retrofit retrofit =
                new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(ErrorHandlerFactory.create())
                        .baseUrl(Config.END_POINT)
                        .client(httpClient.build())
                        .build();
        return retrofit.create(TourTripAPI.class);
    }

    // TODO: 12/06/2017 Fake request
    public static TourTripAPI getFakeRequest(boolean isHeader, boolean isMultiPart) {
        HttpLoggingInterceptor logging = getHttpLoggingInterceptor();
        OkHttpClient client;
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        httpClient.readTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        httpClient.addInterceptor(logging);
        if (isHeader) {
            httpClient.addInterceptor(requestWithHeader());
        }
        if (isMultiPart) {
            httpClient.addInterceptor(requestMultipart());
        }

        Retrofit retrofit =
                new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(ErrorHandlerFactory.create())
                        .baseUrl(Config.END_POINT)
                        .client(httpClient.build())
                        .build();
        return retrofit.create(TourTripAPI.class);
    }

    public static TourTripAPI getPWRequestConnect() {
        HttpLoggingInterceptor logging = getHttpLoggingInterceptor();
        OkHttpClient client;
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        httpClient.readTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        httpClient.addInterceptor(logging);
        Retrofit retrofit =
                new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(ErrorHandlerFactory.create())
                        .baseUrl(Config.END_POINT_RESETPW)
                        .client(httpClient.build())
                        .build();
        return retrofit.create(TourTripAPI.class);
    }

    private static HttpLoggingInterceptor getHttpLoggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY
                : HttpLoggingInterceptor.Level.NONE);
        return interceptor;
    }

    private static Interceptor requestWithHeader() {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                //Token of user when registration is success.
                Request original = chain.request();
                Request.Builder builder = original.newBuilder()
                        .addHeader("Cache-Control", "no-cache")
                        .header("Content-Type", "application/json")
                        .header("Authorization", "Bearer ")
                        .method(original.method(), original.body());
                Response response = chain.proceed(builder.build());
                return response;
            }
        };
    }

    private static Interceptor requestMultipart() {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder builder = original.newBuilder()
                        .addHeader("Content-Type", "multipart/form-data")
                        .header("Authorization", "no")
                        .method(original.method(), original.body());
                Response response = chain.proceed(builder.build());
                return response;
            }
        };
    }
}

