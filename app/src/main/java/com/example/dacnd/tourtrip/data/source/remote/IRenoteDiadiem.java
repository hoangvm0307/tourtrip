package com.example.dacnd.tourtrip.data.source.remote;

import com.example.dacnd.tourtrip.data.model.database.Diadiem;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by Dac ND on 22-Dec-17.
 */

public interface IRenoteDiadiem {
    Observable<ArrayList<Diadiem>> getData();
}
