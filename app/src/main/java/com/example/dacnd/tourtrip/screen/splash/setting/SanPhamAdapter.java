package com.example.dacnd.tourtrip.screen.splash.setting;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.dacnd.tourtrip.R;
import com.example.dacnd.tourtrip.data.model.database.Diadiem;
import com.example.dacnd.tourtrip.screen.splash.map.Map;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Dac ND on 22-Dec-17.
 */

public class SanPhamAdapter extends RecyclerView.Adapter<SanPhamAdapter.ViewHolder> {
    private Context context;
    private int layout;
    private List<Diadiem> sanphamArrayList;

    public SanPhamAdapter(Context context, int layout, List<Diadiem> sanphamArrayList) {
        this.context = context;
        this.layout = layout;
        this.sanphamArrayList = sanphamArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(layout,null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Diadiem sanpham = sanphamArrayList.get(position);
        holder.txttenSanpham.setText(sanpham.getTen_dd());
        holder.txtgiasanpham.setText(sanpham.getSo_phanhoi());
        holder.ratingBar.setRating(Float.parseFloat(sanpham.getDanhgia())/10);
        Picasso.with(context).load(sanpham.getAnh()).placeholder(R.drawable.error)
                .error(R.drawable.error).into(holder.imaghinanhsanpham);
    }

    @Override
    public int getItemCount() {
        return sanphamArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imaghinanhsanpham;
        CardView cardView;
        RatingBar ratingBar;
        public TextView txttenSanpham,txtgiasanpham;
        public ViewHolder(View v) {
            super(v);
            cardView = (CardView) v.findViewById(R.id.carview_id);
            imaghinanhsanpham = (ImageView) v.findViewById(R.id.imageviewsanpham);
            txtgiasanpham = (TextView) v.findViewById(R.id.txtgiasp);
            txttenSanpham = (TextView) v.findViewById(R.id.txttensanpham);
            ratingBar = (RatingBar) v.findViewById(R.id.rating);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, Map.class);
                    intent.putExtra("lat",Double.parseDouble(sanphamArrayList.get(getPosition()).getLat()));
                    intent.putExtra("lon",Double.parseDouble(sanphamArrayList.get(getPosition()).getLon()));
                    intent.putExtra("name",sanphamArrayList.get(getPosition()).getTen_dd());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
        }
    }
}

