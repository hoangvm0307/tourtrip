package com.example.dacnd.tourtrip.screen.splash;

import android.content.Intent;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.dacnd.tourtrip.R;
import com.example.dacnd.tourtrip.base.BaseDataBindActivity;
import com.example.dacnd.tourtrip.databinding.ActivitySplashBinding;
import com.example.dacnd.tourtrip.screen.splash.inputcustomer.InputCustomerActivity;


/**
 * Created by Dac ND on 13-Dec-17.
 */

public class SplashActivity extends BaseDataBindActivity<ActivitySplashBinding,SplashPresenter> implements SplashModel.ViewModel,Animation.AnimationListener{
    ImageView imglogo;
    RelativeLayout layout;
    @Override
    protected int getIdLayout() {
        return R.layout.activity_splash;
    }

    @Override
    protected void initData() {
//        mPresenter = new SplashPresenter(this,this);
        mBinding.setPresenter(mPresenter);
        imglogo = mBinding.icon;
        layout = mBinding.backGroup;
        Animation animationStation = AnimationUtils.loadAnimation(this,R.anim.transition_icon);
        imglogo.setAnimation(animationStation);
        Animation animationapha = AnimationUtils.loadAnimation(this,R.anim.aphal_backgroaud);
        layout.setAnimation(animationapha);
        animationapha.setAnimationListener(this);
    }

    @Override
    public void showLoading() {
        showLoadingDialog();
    }

    @Override
    public void hideLoading() {
        hideLoadingDialog();
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        showLoading();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                hideLoading();
                startActivity(new Intent(SplashActivity.this,InputCustomerActivity.class));
                finish();

            }
        },3000);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
