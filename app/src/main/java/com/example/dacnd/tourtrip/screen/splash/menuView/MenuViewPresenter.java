package com.example.dacnd.tourtrip.screen.splash.menuView;

import android.content.Context;

import com.example.dacnd.tourtrip.R;
import com.example.dacnd.tourtrip.data.model.database.Diadiem;
import com.example.dacnd.tourtrip.data.model.database.Function;
import com.example.dacnd.tourtrip.data.source.remote.service.Api_CLient;
import com.example.dacnd.tourtrip.data.source.remote.service.TourTripAPI;
import com.example.dacnd.tourtrip.widget.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Dac ND on 15-Dec-17.
 */

public class MenuViewPresenter implements MenuViewContact.Presenter {

    List<Diadiem> mang;
    public MenuViewPresenter(Context mContext, MenuViewContact.ViewModel mViewModel) {
        this.mContext = mContext;
        this.mViewModel = mViewModel;
        mang = new ArrayList<>();
        getDulieuSanphammoinhat();
    }

    private Context mContext;
    private MenuViewContact.ViewModel mViewModel;

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {

    }
    public void onCancel(){
        mViewModel.clickback();
    }

    public List<Function> getBottomListItem() {
        List<Function> subMenu = new ArrayList<>();
        subMenu.add(new Function(R.drawable.ic_home_new,1,"Trang chủ","Trang Chủ"));
        subMenu.add(new Function(R.drawable.ic_more_new,2,"Cấu Hình","Địa Điểm"));
        return subMenu;
    }
    public BottomNavigationView.OnBottomItemClick getOnclick() {
        return new BottomNavigationView.OnBottomItemClick() {
            @Override
            public void onBottomItemClick(int position) {
                switch (position){
                    case 0:
                        mViewModel.gotoMenu();
                        break;
                    case 1:
                       mViewModel.next();
                        break;
                }

            }
        };
    }
    private void getDulieuSanphammoinhat() {
        mViewModel.showLoading();
        TourTripAPI api = Api_CLient.getRetrofit().create(TourTripAPI.class);
        Call<ArrayList<Diadiem>> call = api.getLoaisanpham();
        call.enqueue(new Callback<ArrayList<Diadiem>>() {
            @Override
            public void onResponse(Call<ArrayList<Diadiem>> call, Response<ArrayList<Diadiem>> response) {
                if(response != null){
                    for(Diadiem  a : response.body()){
                        mang.add(a);

                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Diadiem>> call, Throwable t) {
            }

        });
        mViewModel.hideLoading();
    }
    public List<Diadiem> updateData(){
        return mang;
    }


}
