package com.example.dacnd.tourtrip.screen.splash.inputcustomer;

import android.content.Context;

/**
 * Created by Dac ND on 14-Dec-17.
 */

public class InputCustomerPresenter implements InputCustomerContact.Presenter {
    private Context mContext;
    private InputCustomerContact.ViewModel mViewmodel;

    public InputCustomerPresenter(Context mContext, InputCustomerContact.ViewModel mViewmodel) {
        this.mContext = mContext;
        this.mViewmodel = mViewmodel;
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {

    }
    public  void onCancel(){
        mViewmodel.ClickBack();
    }
    public void doProcess(){
        mViewmodel.doProcess();
    }

}
