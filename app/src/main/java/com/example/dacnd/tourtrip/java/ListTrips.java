package com.example.dacnd.tourtrip.java;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

/**
 * Created by root on 08/01/2018.
 */

public class ListTrips {

    public static ListTrips listTrips = null;


    public static ListTrips getInstance() {
        if (listTrips == null){
            return new ListTrips();
        }
        return listTrips;
    }

    private ListTrips() {
    }

    public JSONArray getData(String url) {
        JSONArray json_aray = new JSONArray();
        try {
            Connection.Response response = Jsoup.connect(url).method(Connection.Method.GET).execute();

            Elements myitems = response.parse().getElementsByClass("sr_item");
//        System.out.println(myitems.size());


            for (int i = 0; i < myitems.size(); i++) {
                JSONObject json_object = new JSONObject();
                Element hodel1 = myitems.get(i);

//            System.out.println(i);
                System.out.println(hodel1.getElementsByClass("hotel_image").get(0).attr("src").trim());
                json_object.put("hotel_icon", hodel1.getElementsByClass("hotel_image").get(0).attr("src").trim());
                System.out.println(hodel1.getElementsByClass("sr-hotel__name").text());
                json_object.put("hotel_name", hodel1.getElementsByClass("sr-hotel__name").text());
                System.out.println(hodel1.getElementsByClass("address").text());
                json_object.put("hotel_address", hodel1.getElementsByClass("address").text());
                System.out.println(hodel1.getElementsByClass("review-score-badge").get(0).text());
                json_object.put("hotel_ratting", hodel1.getElementsByClass("review-score-badge").get(0).text());


                json_aray.put(json_object);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json_aray;
    }
}