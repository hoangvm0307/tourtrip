package com.example.dacnd.tourtrip.screen.splash.map;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.example.dacnd.tourtrip.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class Map extends FragmentActivity implements OnMapReadyCallback {
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney, Australia, and move the camera.
        LatLng sydney = new LatLng(getLat(), getLaon());
        mMap.addMarker(new MarkerOptions().position(sydney).title(getName()));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,13));

    }
    public double getLat(){
        return getIntent().getDoubleExtra("lat",-1);
    }
    public double getLaon(){
        return getIntent().getDoubleExtra("lon",-1);
    }
    public String getName(){
        return getIntent().getStringExtra("name");
    }
}
