package com.example.dacnd.tourtrip.data.model.database;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dac ND on 22-Dec-17.
 */

public class Diadiem implements Parcelable {
    @SerializedName("lon")
    @Expose
    private String lon;
    @SerializedName("anh")
    @Expose
    private String anh;
    @SerializedName("ten_dd")
    @Expose
    private String ten_dd;
    @SerializedName("danhgia")
    @Expose
    private String danhgia;
    @SerializedName("so_phanhoi")
    @Expose
    private String so_phanhoi;
    @SerializedName("ma_tp")
    @Expose
    private String ma_tp;
    @SerializedName("ma_dd")
    @Expose
    private String ma_dd;
    @SerializedName("ma_hd")
    @Expose
    private String ma_hd;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("diachi")
    @Expose
    private String diachi;

    public String getLon ()
    {
        return lon;
    }

    public void setLon (String lon)
    {
        this.lon = lon;
    }

    public String getAnh ()
    {
        return anh;
    }

    public void setAnh (String anh)
    {
        this.anh = anh;
    }

    public String getTen_dd ()
    {
        return ten_dd;
    }

    public void setTen_dd (String ten_dd)
    {
        this.ten_dd = ten_dd;
    }

    public String getDanhgia ()
    {
        return danhgia;
    }

    public void setDanhgia (String danhgia)
    {
        this.danhgia = danhgia;
    }

    public String getSo_phanhoi ()
    {
        return so_phanhoi;
    }

    public void setSo_phanhoi (String so_phanhoi)
    {
        this.so_phanhoi = so_phanhoi;
    }

    public String getMa_tp ()
    {
        return ma_tp;
    }

    public void setMa_tp (String ma_tp)
    {
        this.ma_tp = ma_tp;
    }

    public String getMa_dd ()
    {
        return ma_dd;
    }

    public void setMa_dd (String ma_dd)
    {
        this.ma_dd = ma_dd;
    }

    public String getMa_hd ()
    {
        return ma_hd;
    }

    public void setMa_hd (String ma_hd)
    {
        this.ma_hd = ma_hd;
    }

    public String getLat ()
    {
        return lat;
    }

    public void setLat (String lat)
    {
        this.lat = lat;
    }

    public String getDiachi ()
    {
        return diachi;
    }

    public void setDiachi (String diachi)
    {
        this.diachi = diachi;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [lon = "+lon+", anh = "+anh+", ten_dd = "+ten_dd+", danhgia = "+danhgia+", so_phanhoi = "+so_phanhoi+", ma_tp = "+ma_tp+", ma_dd = "+ma_dd+", ma_hd = "+ma_hd+", lat = "+lat+", diachi = "+diachi+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.lon);
        dest.writeString(this.anh);
        dest.writeString(this.ten_dd);
        dest.writeString(this.danhgia);
        dest.writeString(this.so_phanhoi);
        dest.writeString(this.ma_tp);
        dest.writeString(this.ma_dd);
        dest.writeString(this.ma_hd);
        dest.writeString(this.lat);
        dest.writeString(this.diachi);
    }

    public Diadiem() {
    }

    protected Diadiem(Parcel in) {
        this.lon = in.readString();
        this.anh = in.readString();
        this.ten_dd = in.readString();
        this.danhgia = in.readString();
        this.so_phanhoi = in.readString();
        this.ma_tp = in.readString();
        this.ma_dd = in.readString();
        this.ma_hd = in.readString();
        this.lat = in.readString();
        this.diachi = in.readString();
    }

    public static final Parcelable.Creator<Diadiem> CREATOR = new Parcelable.Creator<Diadiem>() {
        @Override
        public Diadiem createFromParcel(Parcel source) {
            return new Diadiem(source);
        }

        @Override
        public Diadiem[] newArray(int size) {
            return new Diadiem[size];
        }
    };
}

