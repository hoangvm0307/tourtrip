package com.example.dacnd.tourtrip.screen.splash.menuView.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ViewFlipper;

import com.example.dacnd.tourtrip.R;
import com.example.dacnd.tourtrip.base.BaseDataBindFragment;
import com.example.dacnd.tourtrip.data.RemoteDiadiem;
import com.example.dacnd.tourtrip.data.model.database.Diadiem;
import com.example.dacnd.tourtrip.databinding.MainFragmentBinding;
import com.example.dacnd.tourtrip.screen.splash.setting.SanPhamAdapter;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.nineoldandroids.view.ViewHelper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dac ND on 15-Dec-17.
 */

public class MainFragment extends BaseDataBindFragment<MainFragmentBinding,MainFragmentPresenter>
    implements MainFragmentContact.ViewModel,ObservableScrollViewCallbacks {
    ViewFlipper viewFlipper;
    private View mToolbarView;
    private ObservableScrollView mScrollView;
    private int mParallaxImageHeight;

    public MainFragment(List<Diadiem> mangsanPham) {
        this.mangsanPham = mangsanPham;
    }

    private static List<Diadiem> mangsanPham ;
    RemoteDiadiem remoteDiadiem;
    RecyclerView recyclerview;
    SanPhamAdapter sanphamadapter;



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }


    @Override
    protected void initData() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(mBinding.toolbar);
        mToolbarView = mBinding.toolbar;
        mToolbarView.setBackgroundColor(ScrollUtils.getColorWithAlpha(0, getResources().getColor(R.color.primary)));
        remoteDiadiem = RemoteDiadiem.getInstance();
        mScrollView = mBinding.scrollView;
        mScrollView.setScrollViewCallbacks(this);
        mParallaxImageHeight = getResources().getDimensionPixelSize(R.dimen.parallax_image_height);
        mPresenter = new MainFragmentPresenter(this, getActivity());
        mBinding.setPresenter(mPresenter);
        viewFlipper = mBinding.viewlipper;
        ArrayList<String> mangquangcao = new ArrayList<>();
        mangquangcao.add("http://tuannguyentravel.vn/data/images/da%20nang/du-lich-da-nang(1).jpg");
        mangquangcao.add("http://aprotravel.vn/wp-content/uploads/tour-du-lich-Bai-Dinh-Trang-An.jpg");
        mangquangcao.add("http://grandhoabinhhotel.com/pic/New/images/20.jpg");
        mangquangcao.add("http://moitruong.net.vn/wp-content/uploads/2017/03/dulichtp_w_550.jpg");
        for (int i = 0; i < mangquangcao.size(); i++) {
            ImageView imagview = new ImageView(getActivity());
            Picasso.with(getActivity()).load(mangquangcao.get(i)).
                    into(imagview);
            imagview.setScaleType(ImageView.ScaleType.FIT_XY);
            viewFlipper.addView(imagview);
        }
        viewFlipper.setFlipInterval(5000);
        viewFlipper.setAutoStart(true);
        Animation animation_slide_in = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_right);
        Animation animation_slide_out = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_out_right);
        viewFlipper.setAnimation(animation_slide_in);
        viewFlipper.setAnimation(animation_slide_out);
        recyclerview = mBinding.recyclerview;
        recyclerview.setHasFixedSize(true);
        recyclerview.setLayoutManager(new GridLayoutManager(getActivity(),1,GridLayoutManager.HORIZONTAL,false));
        sanphamadapter = new SanPhamAdapter(getActivity(), R.layout.item_san_pham_moi_nhat, mangsanPham);
        onScrollChanged(mScrollView.getCurrentScrollY(), false, false);
        recyclerview.setAdapter(sanphamadapter);
    }

    @Override
    protected int getIdLayoutRes() {
        return R.layout.main_fragment;
    }

    @Override
    protected void initView() {

    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        int baseColor = getResources().getColor(R.color.primary);
        float alpha = Math.min(1, (float) scrollY / mParallaxImageHeight);
        mToolbarView.setBackgroundColor(ScrollUtils.getColorWithAlpha(alpha, baseColor));
        ViewHelper.setTranslationY(viewFlipper, scrollY / 2);
    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

    }


}
