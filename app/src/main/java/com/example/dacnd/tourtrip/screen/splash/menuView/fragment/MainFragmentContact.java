package com.example.dacnd.tourtrip.screen.splash.menuView.fragment;

import com.example.dacnd.tourtrip.base.BasePresenter;
import com.example.dacnd.tourtrip.base.BaseView;

/**
 * Created by Dac ND on 15-Dec-17.
 */

public class MainFragmentContact {
    interface Presenter extends BasePresenter{

    }
    interface ViewModel extends BaseView<Presenter>{

    }
}
