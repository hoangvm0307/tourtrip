package com.example.dacnd.tourtrip.screen.splash;


import android.content.Context;

/**
 * Created by Dac ND on 13-Dec-17.
 */

public class SplashPresenter implements SplashModel.Presenter {
    private Context mContext;
    private SplashModel.ViewModel mViewModel;

    public SplashPresenter(Context mContext, SplashModel.ViewModel mViewModel) {
        this.mContext = mContext;
        this.mViewModel = mViewModel;
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {

    }
}
