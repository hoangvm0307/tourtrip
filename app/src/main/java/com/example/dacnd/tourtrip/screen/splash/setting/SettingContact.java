package com.example.dacnd.tourtrip.screen.splash.setting;

import com.example.dacnd.tourtrip.base.BasePresenter;
import com.example.dacnd.tourtrip.base.BaseView;
import com.example.dacnd.tourtrip.data.model.database.Diadiem;

/**
 * Created by Dac ND on 16-Dec-17.
 */

public class SettingContact {
    interface Prsenter extends BasePresenter{

    }
    interface ViewModel extends BaseView<Prsenter>{

        void goTomap(Diadiem diadiem);
    }
}
