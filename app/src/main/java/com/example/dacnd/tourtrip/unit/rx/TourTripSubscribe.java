package com.example.dacnd.tourtrip.unit.rx;

import android.app.Activity;
import android.util.Log;

import com.example.dacnd.tourtrip.data.source.remote.response.BaseException;

import rx.Subscriber;

/**
 * Created by Dac ND on 15-Dec-17.
 */

public abstract class TourTripSubscribe<T> extends Subscriber<T> {

    private Activity mContext;

    public TourTripSubscribe() {
        super();
    }

    public TourTripSubscribe(Activity context) {
        super();
        mContext = context;
    }

    private T object;

    @Override
    public void onCompleted() {
        onRequestFinish();
        onSuccess(object);
    }

    @Override
    public void onError(Throwable e) {
        onRequestFinish();
        BaseException exception;
        if (e instanceof BaseException) {
            exception = (BaseException) e;
        } else {
            exception = BaseException.toUnexpectedError(e);
        }
        if (exception != null) {
            if (exception.getServerErrorCode().equals("S401") || exception.getServerErrorCode().equals("401")) {
                Log.d("SPECC", "auto logout: " + exception.getServerErrorCode());

            }
        }
        onError(exception);
    }

    @Override
    public void onNext(T t) {
        object = t;
    }

    public abstract void onSuccess(T object);

    public abstract void onError(BaseException error);

    /**
     * Runs after request complete or error
     **/
    public void onRequestFinish() {

    }
}


