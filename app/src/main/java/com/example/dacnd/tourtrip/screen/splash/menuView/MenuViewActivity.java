package com.example.dacnd.tourtrip.screen.splash.menuView;

import android.content.Intent;
import android.support.v4.app.Fragment;

import com.example.dacnd.tourtrip.R;
import com.example.dacnd.tourtrip.base.BaseDataBindActivity;
import com.example.dacnd.tourtrip.databinding.ActivityMenuViewBinding;
import com.example.dacnd.tourtrip.screen.splash.menuView.fragment.MainFragment;
import com.example.dacnd.tourtrip.screen.splash.setting.SettingActivity;

/**
 * Created by Dac ND on 15-Dec-17.
 */

public class MenuViewActivity extends BaseDataBindActivity<ActivityMenuViewBinding,MenuViewPresenter>
    implements MenuViewContact.ViewModel{
    @Override
    protected int getIdLayout() {
        return R.layout.activity_menu_view;
    }

    @Override
    protected void initData() {
        mPresenter = new MenuViewPresenter(this,this);
        mBinding.setPresenter(mPresenter);
        initView();
    }
    private void initView() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.main, new MainFragment(mPresenter.updateData()))
                .commit();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void clickback() {
        finish();
    }

    @Override
    public void gotoMenu() {
        Fragment fragment = getSupportFragmentManager().
            findFragmentById(R.id.main);
        if (fragment != null && fragment instanceof MainFragment) return;
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.main, new MainFragment(mPresenter.updateData()))
                .addToBackStack(MainFragment.class.getSimpleName())
                .commit();
    }
    @Override
    public void next() {
        startActivity(new Intent(MenuViewActivity.this, SettingActivity.class));
    }
}
