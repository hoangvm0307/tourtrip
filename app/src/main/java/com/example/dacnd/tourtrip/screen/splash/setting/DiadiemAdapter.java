package com.example.dacnd.tourtrip.screen.splash.setting;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.dacnd.tourtrip.R;
import com.example.dacnd.tourtrip.data.model.database.Diadiem;
import com.example.dacnd.tourtrip.databinding.ItemSanPhamMoiNhatBinding;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Dac ND on 23-Dec-17.
 */

public class DiadiemAdapter extends RecyclerView.Adapter<DiadiemAdapter.ViewHolder> {

    private Context context;
    private ItemSanPhamMoiNhatBinding binding;
    private List<Diadiem> itemsList;
    private ItemListener listener;

    public DiadiemAdapter(Context context, List<Diadiem> itemsList) {
        this.itemsList = itemsList;
        this.context = context;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        binding =
                ItemSanPhamMoiNhatBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(itemsList.get(position), position);

    }

    @Override
    public int getItemCount() {
        return itemsList == null ? 0 : itemsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ItemSanPhamMoiNhatBinding itemBinding;
        private Diadiem item;
        private int position;

        public ViewHolder(ItemSanPhamMoiNhatBinding itemView) {
            super(itemView.getRoot());
            itemBinding = itemView;

        }

        public void bind(final Diadiem itemReturn, int position) {
            itemBinding.setSanphammoinhat(itemReturn);
            this.position = position;
            this.item = itemReturn;
            itemBinding.rating.setRating(Float.parseFloat(itemReturn.getDanhgia())/10);
            Picasso.with(context).load(itemReturn.getAnh()).placeholder(R.drawable.bg_loading)
                    .error(R.drawable.error).into(itemBinding.imageviewsanpham);

            itemBinding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onClick(getAdapterPosition(),
                                itemReturn);
                    }
                }
            });
        }

        public void onClickItem() {
            if (listener != null) {
                listener.onClick(position, item);
            }
        }
    }

    public interface ItemListener {
        void onClick(int position, Diadiem item);
    }

    public void setItemListemer(ItemListener callback) {
        this.listener = callback;
    }
}


