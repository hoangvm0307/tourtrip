package com.example.dacnd.tourtrip.data.source.remote.service;

import com.example.dacnd.tourtrip.BuildConfig;
import com.example.dacnd.tourtrip.config.Config;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Dac ND on 22-Dec-17.
 */

public class Api_CLient {

    private static Retrofit retrofit = null;
    private static final int CONNECTION_TIMEOUT = 30;

    public static  Retrofit getRetrofit(){
        if(retrofit == null){
            HttpLoggingInterceptor logging = getHttpLoggingInterceptor();
            OkHttpClient client;
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS);
            httpClient.readTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS);
            httpClient.addInterceptor(logging);
            retrofit = new Retrofit.Builder()
                            .addConverterFactory(GsonConverterFactory.create())
                            .baseUrl(Config.END_POINT)
                            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                            .client(httpClient.build())
                            .build();
        }
        return  retrofit;
    }
    private static HttpLoggingInterceptor getHttpLoggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY
                : HttpLoggingInterceptor.Level.NONE);
        return interceptor;
    }

}
